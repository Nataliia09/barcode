import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'barcode';
  private barcodes = [];
  private inputs = [];

  ngOnInit() {
    this.detectByPrefix();
  }

  onChange(event) {
    this.inputs.push(event.target.value);
  }


  detectByPrefix() {
    let prefix = '';
    let code = '';
    let count = 0;
    let reading = false;
    document.addEventListener('keypress', (e: any) => {
     console.log(e.target.localName);
     if(e.target.localName !== 'input') {
      e.preventDefault();
     }
       if(count < 3) {
         prefix += e.key;
         count++;
       } else {
         if (prefix === 'SSS' && e.keyCode !== 13) {
          reading = true;
          code += e.key;
          count++;
         } else {
           console.log(code);
           this.barcodes.push(code);
           prefix = '';
           code = '';
           count = 0;
           reading = false;
         }
       }
    })

    if (reading) {
      reading = false;
      setTimeout(() => {
        prefix = '';
        code = '';
        count = 0;
        reading = false;
      }, 200);
    }
  }


}
